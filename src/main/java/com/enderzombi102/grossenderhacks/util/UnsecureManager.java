package com.enderzombi102.grossenderhacks.util;

import com.enderzombi102.grossenderhacks.entrypoint.BeforeExit;
import net.fabricmc.loader.api.FabricLoader;

import java.security.Permission;
import java.util.Arrays;

import static com.enderzombi102.grossenderhacks.Const.EVENT_LOGGER;

@SuppressWarnings("removal")
public class UnsecureManager extends SecurityManager {
	@Override
	public void checkExit( int status ) {
		EVENT_LOGGER.info("Before exit");

		// ignore our own call
		if (
				Arrays.stream(
						Thread.currentThread().getStackTrace()
				).anyMatch( elem -> elem.getClassName().equals( this.getClass().getName() ) )
		) return;

		var ctx = new BeforeExit.ExitContext( status );
		try {
			FabricLoader.getInstance()
					.getEntrypoints("geh:beforeExit", BeforeExit.class)
					.forEach( entrypoint -> entrypoint.onExit(ctx) );
		} catch ( Exception ignored ) {}

		if ( ctx.isShutdownAllowed() ) {
			super.checkExit( ctx.getExitCode() );
			// if we are here then we are allowed to exit
			System.exit( ctx.getExitCode() );
		} else {
			throw new SecurityException("No, u can't exit");
		}
	}

	public void checkPermission( Permission perm ) {
		if (  perm.getName().equals( "exit" ) )
			java.security.AccessController.checkPermission(perm);
	}

}
