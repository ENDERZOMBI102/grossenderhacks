package com.enderzombi102.grossenderhacks.util;

import com.google.common.collect.Lists;
import org.quiltmc.loader.api.QuiltLoader;

import java.io.File;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.util.List;
import java.util.Locale;

public class ManagementUtil {
	private static final String PATH_SEPARATOR = File.pathSeparator;
	private static final String SEPARATOR = File.separator;
	private static final RuntimeMXBean RUNTIME = ManagementFactory.getRuntimeMXBean();

	public static List<String> classPath() {
		return Lists.newArrayList( RUNTIME.getClassPath().split(PATH_SEPARATOR) );
	}

	public static List<String> libraryPath() {
		return Lists.newArrayList( RUNTIME.getLibraryPath().split(PATH_SEPARATOR) );
	}

	public static String jvmPath() {
		return System.getProperty("java.home") + SEPARATOR + "bin" + SEPARATOR + "javaw" + (
				System.getProperty("os.name").toLowerCase(Locale.ROOT).contains("windows") ? ".exe" : ""
		);
	}

	public static List<String> jvmArguments() {
		return RUNTIME.getInputArguments();
	}

	public static String jvmPid() {
		return String.valueOf( RUNTIME.getPid() );
	}

	public static List<String> appArguments() {
		return List.of( QuiltLoader.getLaunchArguments( false ) );
	}
}
