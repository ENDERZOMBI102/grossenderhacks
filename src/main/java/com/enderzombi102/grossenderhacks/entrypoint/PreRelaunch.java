package com.enderzombi102.grossenderhacks.entrypoint;

import com.enderzombi102.grossenderhacks.relauncher.RelauncherContext;

// before relaunch
public interface PreRelaunch {
	void onPreRelaunch( RelauncherContext ctx );
}
