package com.enderzombi102.grossenderhacks.entrypoint;

// security manager
/**
 * Called before jvm exit ( on personal thread )
 */
public interface BeforeExit {
	void onExit( ExitContext ctx );

	class ExitContext {
		private boolean allowShutdown = true;
		private int exitCode;

		public ExitContext( int exitCode ) {
			this.exitCode = exitCode;
		}

		/**
		 * Prevents the jvm to shut down
		 */
		public void disallowShutdown() {
			allowShutdown = false;
		}

		/**
		 * Set the JVM's exit code
		 */
		public void setExitCode( int code ) {
			exitCode = code;
		}

		public int getExitCode() {
			return exitCode;
		}

		public boolean isShutdownAllowed() {
			return allowShutdown;
		}
	}
}
