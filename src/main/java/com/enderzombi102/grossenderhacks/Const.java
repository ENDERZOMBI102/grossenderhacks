package com.enderzombi102.grossenderhacks;

import org.quiltmc.loader.api.QuiltLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;

public final class Const {
	public static final Logger EVENT_LOGGER = LoggerFactory.getLogger("Events");
	public static final String MOD_VERSION = "0.1.0";
	public static final Path JAR_PATH = (
		( Callable<Path> ) () -> {
			try {
				if ( QuiltLoader.isDevelopmentEnvironment() ) {
					return QuiltLoader.getGameDir().toAbsolutePath().resolve( "../build/devlibs/GrossEnderHacks-" + MOD_VERSION + "-dev.jar" ).toRealPath();
				} else {
					return Path.of( Const.class.getProtectionDomain().getCodeSource().getLocation().toURI() );
				}
			} catch ( URISyntaxException | IOException e ) {
				throw new RuntimeException(e);
			}

		}
	).call();

	@FunctionalInterface
	private interface Callable<T> {
		T call();
	}
}
