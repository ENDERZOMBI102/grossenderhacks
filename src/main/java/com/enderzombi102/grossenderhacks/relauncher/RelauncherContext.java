package com.enderzombi102.grossenderhacks.relauncher;

import com.enderzombi102.grossenderhacks.util.ManagementUtil;
import net.fabricmc.loader.api.FabricLoader;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

/**
 * Context for pre-relaunch entrypoints
 */
@SuppressWarnings("unused")
public final class RelauncherContext {
	final List<String> OPENS = new ArrayList<>() {{
		addOpens( "java.management", "sun.management" );
		addOpens( "java.management", "com.sun.management.internal" );
		addOpens( "java.management", "com.sun.jmx.mbeanserver" );
		addOpens( "java.base", "jdk.internal.reflect" );
	}};
	final List<String> EXPORTS = new ArrayList<>() {{
		addExports( "java.management", "sun.management" );
		addExports( "java.management", "com.sun.management.internal" );
		addExports( "java.management", "com.sun.jmx.mbeanserver" );
		addExports( "java.base", "jdk.internal.reflect" );
	}};
	final List<String> PATCHES = new ArrayList<>();
	final List<String> READS = new ArrayList<>();
	final List<String> MODULES = new ArrayList<>();
	final List<String> CLASS_PATH = ManagementUtil.classPath();
	final List<String> LIBRARY_PATH = ManagementUtil.libraryPath();
	final List<String> JVM_ARGUMENTS = ManagementUtil.jvmArguments();
	final List<String> MAIN_ARGUMENTS = List.of( FabricLoader.getInstance().getLaunchArguments(false) );
	private boolean dirtyClassPath = false;
	private boolean dirtyLibraryPath = false;

	/**
	 * Adds an --add-opens parameter to the launching JVM
	 * <br><i>--add-opens makes all classes in a package open for reflective access.</i>
	 * @param module module to export from
	 * @param pkg package to make open
	 */
	public void addOpens( String module, String pkg ) {
		OPENS.add( "--add-opens $module/$pkg=ALL-UNNAMED" );
	}

	/**
	 * Adds an --add-exports parameter to the launching JVM
	 * <br><i>--add-exports makes all classes in a package open for access.</i>
	 * @param module module to export from
	 * @param pkg package to export
	 */
	public void addExports( String module, String pkg ) {
		EXPORTS.add( "--add-exports $module/$pkg=ALL-UNNAMED" );
	}

	/**
	 * Adds an --patch-module parameter to the launching JVM
	 * <br><i>--patch-module adds to a module classes of a jar.</i>
	 * @param module module to add the jar to
	 * @param jar jar to add
	 */
	public void addPatch( String module, String jar ) {
		PATCHES.add( "--patch-module $module=$jar" );
	}

	/**
	 * Adds an --add-reads parameter to the launching JVM
	 * <br><i>--add-reads allows $module to access all public types in packages exported by $targets modules.</i>
	 * @param module module to add read access to
	 * @param targets comma-separated list of target modules
	 */
	public void addReads( String module, String targets ) {
		READS.add( "--add-reads $module=$targets" );
	}

	/**
	 * Adds an --add-modules parameter to the launching JVM
	 * <br><i>--add-modules allows explicitly defining a comma-separated list of root modules beyond the initial module. .</i>
	 * @param modules comma-separated list of modules
	 */
	public void addModules( String modules ) {
		MODULES.add( "--add-modules $modules" );
	}

	/**
	 * Appends a jar to the new JVM's classpath
	 * @param jar path to the jar to be added
	 * @throws IllegalArgumentException if the given path doesn't point to an existing file
	 */
	public void addClassPathEntry( String jar ) {
		this.addClassPathEntry( jar, true );
	}

	/**
	 * Adds a jar to the new JVM's classpath
	 * @param jar path to the jar to be added
	 * @param append if true, append the jar, else prepend it
	 * @throws IllegalArgumentException if the given path doesn't point to an existing file
	 */
	public void addClassPathEntry( String jar, boolean append ) {
		if (! Path.of( jar ).toFile().exists() )
			throw new IllegalArgumentException("The given path MUST exist on disk");
		this.dirtyClassPath = true;
		if ( append )
			CLASS_PATH.add( jar );
		else
			CLASS_PATH.add( 0, jar );
	}

	/**
	 * Appends a folder to the new JVM's library path
	 * @param folder folder to add
	 * @throws IllegalArgumentException if the given path doesn't point to an existing file
	 */
	public void addLibraryPathEntry( String folder ) {
		this.addLibraryPathEntry( folder, true );
	}

	/**
	 * Adds a folder to the new JVM's library path
	 * @param folder folder to add
	 * @param append if true, append the jar, else prepend it
	 * @throws IllegalArgumentException if the given path doesn't point to an existing file
	 */
	public void addLibraryPathEntry( String folder, boolean append ) {
		var file = Path.of( folder ).toFile();
		if (! ( file.isDirectory() && file.exists() ) )
			throw new IllegalArgumentException("The given path MUST be an existing folder on disk");
		this.dirtyLibraryPath = true;
		if ( append )
			LIBRARY_PATH.add( folder );
		else
			LIBRARY_PATH.add( 0, folder );
	}

	/**
	 * Returns whether the class path was edited
	 */
	public boolean isClassPathDirty() {
		return dirtyClassPath;
	}

	/**
	 * Returns whether the library path was edited
	 */
	public boolean isLibraryPathDirty() {
		return dirtyLibraryPath;
	}
}
