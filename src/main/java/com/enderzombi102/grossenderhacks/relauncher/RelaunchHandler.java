package com.enderzombi102.grossenderhacks.relauncher;

import com.enderzombi102.grossenderhacks.entrypoint.PreRelaunch;
import net.fabricmc.loader.api.FabricLoader;

import java.util.List;

public class RelaunchHandler {

	private final List<PreRelaunch> PRE_LISTENERS;

	public RelaunchHandler() {
		// find listeners
		this.PRE_LISTENERS = FabricLoader.getInstance().getEntrypoints( "geh:pre_relaunch", PreRelaunch.class );
	}

	public boolean needRelaunch() {
		return ! this.PRE_LISTENERS.isEmpty();
	}

	public void doEvent() {
		var ctx = new RelauncherContext();

		for ( var listener : PRE_LISTENERS ) {
			listener.onPreRelaunch(ctx);
		}
	}

	public void relaunch() {
	}
}
