package com.enderzombi102.grossenderhacks;

import com.enderzombi102.enderlib.reflection.Reflection;
import com.enderzombi102.grossenderhacks.util.ManagementUtil;
import com.enderzombi102.grossenderhacks.util.UnsecureManager;
import org.quiltmc.loader.api.LanguageAdapter;
import org.quiltmc.loader.api.ModContainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GrossEnderHacks implements LanguageAdapter {
	private static final Logger LOGGER = LoggerFactory.getLogger("GrossEnderHacks");

	@Override
	public native <T> T create( ModContainer mod, String value, Class<T> type );

	static {
		// log funi stuff
		LOGGER.info( "Oh my, QuiltLoader actually loaded me, worse mistake of its life! lets take over the jvm shall we?" );
		LOGGER.info( "I've found some cool info! want to see?" );
		LOGGER.info( " - library path: " + ManagementUtil.libraryPath() );
		LOGGER.info( " - class path: " + ManagementUtil.classPath() );
		LOGGER.info( " - JVM path: " + ManagementUtil.jvmPath() );
		LOGGER.info( " - JVM Arguments: " + ManagementUtil.jvmArguments() );
		LOGGER.info( " - App Arguments: " + ManagementUtil.appArguments() );

		Reflection.attachAgent( Const.JAR_PATH.toString() );

		// shutdown hook
		System.setSecurityManager( new UnsecureManager() );

	}
}
