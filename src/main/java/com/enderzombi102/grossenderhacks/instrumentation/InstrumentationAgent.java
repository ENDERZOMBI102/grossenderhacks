package com.enderzombi102.grossenderhacks.instrumentation;


import java.lang.instrument.Instrumentation;

public class InstrumentationAgent {
	public static void premain( String agentArgs, Instrumentation inst ) {
		System.out.println("Before main");
	}
	public static void agentmain( String agentArgs, Instrumentation inst ) {
		System.out.println("On agent main");
	}
}
