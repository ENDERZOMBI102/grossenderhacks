package com.enderzombi102.grossenderhacks;

import net.fabricmc.loader.api.entrypoint.PreLaunchEntrypoint;

public class EarlyInit implements PreLaunchEntrypoint {
	@Override
	public void onPreLaunch() {
		System.out.println("on PreLaunch");
	}

	static {
		System.out.println("static PreLaunch");
	}
}
